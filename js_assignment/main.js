console.log(window);
console.error('Error Msg Test');
console.warn('Warning Msg Test');
console.log(document.querySelector('#user-data'))

const nama = document.querySelector('#name');
const age = document.querySelector('#age');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');
const btn = document.querySelector('.btn');
//const userForm = document.querySelector('#user-data');


//mengecek penekanan tombol submit
btn.addEventListener('click', onSubmit)

function onSubmit(e){
    e.preventDefault();

    if(nama.value===''|| age.value==='' || age.value<=0){
        //munculkan error
        msg.classList.add('error');
        msg.textContent = 'Please fill out all fields';

        //hilangkan pesan error
        setTimeout(() => msg.innerHTML = '', 2000);
        setTimeout(() => msg.classList.remove('error'), 2000);
        console.error('One of the form is invalid');
    }else{
        //buat sebuah element list
        const li = document.createElement('li');
        //isi list
        li.appendChild(document.createTextNode(`${nama.value}: ${age.value}`));
        //masukkan li ke dalam ul agar muncul pada web
        userList.appendChild(li);

        console.log('User Added');
    }
}


const ul = document.querySelector('.my-ul')


function button1(){
    //mengubah konten Text li dari Item 1 menjadi Hello
    ul.firstElementChild.textContent = "<h1>Hello</h1>";
    console.log('Text Content changed');
}

function button2(){
    //
    ul.children[1].innerHTML = '<h1>World!</h1>';
    console.log('Element modified');
}

function button3(){
    if(ul.children[3] == null){
        console.error('Removal failure/Element already removed');
    }else{
    ul.children[3].remove();
    console.log('Removal Success');
    }
}

function button5(){
    ul.lastElementChild.remove();
    console.log('Last element deleted');
}