package main

import (
	"fmt"
	welcome "privy-go/helper"
	"strings"
	//"sync"
)

const confName string = "Go Conferrence"

var ticketStock uint = 50

type UserData struct {
	firstName      string
	lastName       string
	email          string
	location       string
	orderedTickets uint
}

var userOrder = make([]UserData, 0) //declares empty list of UserData struct

//var wg = sync.WaitGroup{}

func main() {
	var userTicket uint
	var firstName string
	var lastName string
	var userEmail string
	var location string

	//==========================================================================
	welcome.WelcomeMsg(confName)

	//makes the application to enter an infinite loops, stops by pressing ctrl+c on terminal
	for {
		fmt.Printf("Please enter your First Name: ")
		fmt.Scan(&firstName) //scans user input for firstName then stores it into the memory to be accessed later)
		fmt.Printf("Please enter your Last Name: ")
		fmt.Scan(&lastName)
		fmt.Printf("Please enter your email address: ")
		fmt.Scan(&userEmail)
		fmt.Printf("Which location do you want to go? (Japan/Korea)")
		fmt.Scan(&location)

		//checks inputs
		isValidName, isValidEmail, isValidLoc := validCheck(firstName, lastName, userEmail, location)

		if isValidName && isValidEmail && isValidLoc {

			//sends out additional message based on chosen location
			switch strings.ToLower(location) {
			case "japan",
				"jepang":
				fmt.Println("The show will be held in Tokyo, see you there!")
			case "korea":
				fmt.Println("The show will be held in Seoul, see you there!")
			}

			fmt.Printf("How many tickets you want to order? ")
			fmt.Scan(&userTicket)

			if userTicket > ticketStock {
				fmt.Printf("We only have %v tickets remaining and you ordered for %v tickets, please try again\n\n", ticketStock, userTicket)
				continue
			} else if userTicket <= 0 {
				fmt.Printf("\nCannot order 0 or lower ticket, try again\n")
				continue
			}

			//create a userData struct
			var userData = UserData{
				firstName:      firstName,
				lastName:       lastName,
				email:          userEmail,
				location:       location,
				orderedTickets: userTicket,
			}

			//substract ticketStock with valid userTicket input
			ticketStock = ticketStock - userTicket

			//appends userData into the empty list userOrder
			userOrder = append(userOrder, userData)

			firstNames := getFirstNames()

			fmt.Printf("Here's our list of order(s) Right Now:\n %v", userOrder)

			/*go*/
			ticketDetails(userTicket, firstName, lastName, userEmail, location)

			fmt.Printf("We still have %v tickets left!\n\n", ticketStock)
			fmt.Printf("Here's our list of orderers First Names:\n %v\n\n", firstNames)

			if ticketStock == 0 {
				fmt.Println("Tickets are now sold out! Thank your for your enthusiasm!")
				break
			}
		} else {
			if !isValidName {
				fmt.Println("Name input is invalid")
			}
			if !isValidEmail {
				fmt.Println("Email input is invalid")
			}
			if !isValidLoc {
				fmt.Println("The show is only available in Japan and Korea")
			}
		}
	}
}
