package welcome

import "fmt"

func WelcomeMsg(name string) {
	fmt.Printf("Welcome to the %v Privy GO Language Assignment!\n", name)
}
