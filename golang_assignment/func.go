package main

import (
	"fmt"
	"strings"
	"time"
)

func validCheck(f string, l string, e string, n string) (bool, bool, bool) {
	isValidName := len(f) > 2 && len(l) > 2 //boolean variable
	isValidEmail := strings.Contains(e, "@") && len(e) > 2
	isValidCity := strings.ToLower(n) == "japan" || strings.ToLower(n) == "jepang" || strings.ToLower(n) == "korea"

	return isValidName, isValidEmail, isValidCity
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, userOrder := range userOrder {
		firstNames = append(firstNames, userOrder.firstName)
	}
	return firstNames
}

func ticketDetails(userTickets uint, firstName string, lastName string, email string, city string) {
	time.Sleep(5 * time.Second) //will cause the program to start a 5 secon sleep state in which any actions will be blocked during the whole sleep time, use go before the function call in main to allow this process to be run as a background process
	var ticket = fmt.Sprintf("Name: %v %v\nShow Location: %v \nNumber of Tickets: %v \n", firstName, lastName, city, userTickets)
	fmt.Println("*******************************************************************************************")
	fmt.Printf("Sending ticket with the following details:\n%v\nTo the following email: %v\n", ticket, email)
	fmt.Println("*******************************************************************************************")
}
